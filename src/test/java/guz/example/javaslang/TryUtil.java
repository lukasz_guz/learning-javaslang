package guz.example.javaslang;

import javaslang.Tuple2;
import javaslang.control.Try;

import static javaslang.API.Case;
import static javaslang.API.Match;
import static javaslang.Predicates.instanceOf;

/**
 * Created by lukasz on 13.10.16.
 */
public class TryUtil {

    public static Try<Integer> tryWithRecovery(Try<Integer> tryObject, Tuple2<Class, Integer> returnResult, Tuple2<Class, Integer> other) {
        return tryObject.recover(throwable -> Match(throwable).<Integer>of(
                Case(instanceOf(returnResult._1()), returnResult._2()),
                Case(instanceOf(other._1()), other._2)
                )
        );
    }

    public static Try<Integer> divide(java.lang.Integer a, java.lang.Integer b) {
        return Try.of(() -> a / b);
    }
}
