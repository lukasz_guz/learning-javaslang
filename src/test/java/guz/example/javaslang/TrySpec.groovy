package guz.example.javaslang

import javaslang.Tuple
import javaslang.control.Try
import spock.lang.Specification

import java.util.function.Function

import static guz.example.javaslang.TryUtil.divide
import static guz.example.javaslang.TryUtil.tryWithRecovery

/**
 * Created by lukasz on 13.10.16.
 */
class TrySpec extends Specification {

    def "Should return 1"() {
        expect:
        divide(1, 1).get() == 1
    }

    def "Should throw Exception"() {
        when:
        divide(1, 0).get()

        then:
        Try.NonFatalException e = thrown Try.NonFatalException
        e.cause instanceof ArithmeticException
    }

    def "Should throw NullPointerException replace with ArithmeticException"() {
        when:
        divide(1, 0).getOrElseThrow({ e -> throw new NullPointerException() } as Function)

        then:
        thrown NullPointerException
    }

    def "Should recover on 5 when throw exception"() {
        expect:
        divide(1, 0).recover({ exception -> 5 }).get() == 5
    }

    def "Should recover on Try object when throw exception"() {
        expect:
        divide(1, 0).recoverWith({ exception -> Try.of({ 5 }) }).get() == 5
    }

    def "Should return correct value"() {
        expect:
        tryWithRecovery(divide(1, 0), Tuple.of(NullPointerException, 2), Tuple.of(ArithmeticException, 3)).get() == 3
    }
}