package guz.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaslangLearningApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaslangLearningApplication.class, args);
	}
}
