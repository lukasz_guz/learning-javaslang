package guz.example;

import com.aol.cyclops.trycatch.Try;

import static java.util.Optional.of;

/**
 * Created by lukasz on 28.08.16.
 */
public class TryCyclops {

    public static void main(String[] args) {
        int div = Try.withCatch(() -> divided(1, 0))
                .recoverFor(MyException.class, e ->
                        of(e)
                                .map(e1 -> e1.getMessage())
                                .filter("elo"::equals)
                                .map(s -> 15)
                                .orElse(14)
                )
                .get();
        System.out.println(div);

        System.out.println("elo");
    }


    static int divided(int a, int b) {
        throw new MyException("elo1");
//        return a / b;
    }

    static class MyException extends RuntimeException {
        public MyException(String message) {
            super(message);
        }
    }
}
