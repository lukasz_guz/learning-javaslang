package guz.example.domain;

import com.sun.org.apache.xpath.internal.operations.String;
import lombok.Data;

/**
 * Created by lukasz on 28.08.16.
 */
@Data
public class PersonEntity {

    private String firstName;
    private String lastName;
}
