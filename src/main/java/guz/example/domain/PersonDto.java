package guz.example.domain;

import lombok.Data;

/**
 * Created by lukasz on 28.08.16.
 */
@Data
public class PersonDto {

    private String firstName;
    private String lastName;
}
